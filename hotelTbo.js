const fs = require("fs");
const Hotel = require("./models/Hotels");
const db = require("./config/database");

let rawdata = fs.readFileSync("TBO.json", "utf8");
let splittedRawData = rawdata.split("\n");
let dataHotel = splittedRawData
  .map(function(value) {
    if (value === "") {
      return null;
    }

    value = JSON.parse(value);
    // console.log(value.hotel_mapping_data.hotel_name)
    return {
      bindId: value.bind_id,
      hotelCode: value.hotel_code,
      hotelPlaceCountryCode: value.hotel_place_country_code,
      hotelSupplierCode: value.hotel_supplier_code,
      hotelPlaceDestinationCode: value.hotel_place_destination_code,
      hotelName: value.hotel_mapping_data.hotel_name
    };
  })
  .filter(value => value !== null);

var obj = {};

for (var i = 0, len = dataHotel.length; i < len; i++) {
  obj[`${dataHotel[i]["hotelSupplierCode"]}___${dataHotel[i]["hotelCode"]}`] =
    dataHotel[i];
}

const uniqData = [];
for (var key in obj) {
  uniqData.push(obj[key]);
}
// console.log(uniqData);

Hotel.bulkCreate(uniqData, {
  fields: [
    "hotelSupplierCode",
    "hotelCode",
    "hotelPlaceCountryCode",
    "bindId",
    "hotelPlaceDestinationCode",
    "hotelName"
  ],
  updateOnDuplicate: [
    "hotelPlaceCountryCode",
    "bindId",
    "hotelPlaceDestinationCode",
    "hotelName"
  ]
  // returning: true
})
  .then(() => {
    console.log("selesai");
  })
  .catch(err => {
    console.log(err.message);
  });
