const Sequelize = require("sequelize");
const db = require("../config/database");

const Hotel = db.define(
  "hotels",
  {
    hotelSupplierCode: {
      field: "hotel_supplier_code",
      type: Sequelize.STRING,
      primaryKey: true
    },
    hotelCode: {
      field: "hotel_code",
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    hotelPlaceCountryCode: {
      field: "hotel_place_country_code",
      type: Sequelize.STRING
    },
    bindId: {
      field: "bind_id",
      type: Sequelize.STRING
    },
    hotelPlaceDestinationCode: {
      field: "hotel_place_destination_code",
      type: Sequelize.STRING
    },
    hotelName: {
      field: "hotel_name",
      type: Sequelize.STRING
    }
  },
  {
    freezeTableName: true
  }
);
// Hotel.removeAttribute('id')
module.exports = Hotel;
