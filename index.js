const express = require("express");
const exphbs = require("express-handlebars");
const bodyParser = require("body-parser");
const path = require("path");
const cors = require('cors')

const db = require("./config/database");

// Test DB
db.authenticate()
  .then(() => console.log("database connected"))
  .catch(err => console.log("Error" + err));

const app = express();

app.use(cors());
// Handlebars
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("View engine", "handlebars");

// Body Parser
app.use(bodyParser.urlencoded({ extended: false }));

// Set static folder
app.use(express.static(path.join(__dirname, "public")));

app.get("/", (req, res) => res.send("INDEX"));

app.use('/hotels', require('./routes/hotels'))

const PORT = process.env.PORT || 3010;

app.listen(PORT, console.log(`Server started on port ${PORT}`));
