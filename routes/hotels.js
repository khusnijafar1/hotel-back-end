const express = require("express");
const router = express.Router();
const sequelize = require("sequelize");
const db = require("../config/database");
const Hotel = require("../models/Hotels");

router.get("/getSupplier", (req, res) => {
  Hotel.findAll({
    attributes: [
      [
        sequelize.fn("DISTINCT", sequelize.col("hotel_supplier_code")),
        "hotelSupplierCode"
      ]
    ]
  }).then(hotels => {
    res.json(hotels);
  });
});

router.get("/getSupplierCode/:supplier_code", (req, res) => {
  // console.log(req.params.id)
  Hotel.findAll({
    where: { hotelSupplierCode: req.params.supplier_code }
    // limit: 10
  })
    .then(hotels => {
      res.json(hotels);
    })
    .catch(err => console.log(err));
});

router.get("/getCountry/:supplier_code", (req, res) => {
  Hotel.findAll({
    attributes: [
      [
        sequelize.fn("DISTINCT", sequelize.col("hotel_place_country_code")),
        "hotelPlaceCountryCode"
      ]
    ],
    where: { hotelSupplierCode: req.params.supplier_code }
  }).then(hotels => {
    res.json(hotels);
  });
});

router.get("/getCountryCode/:country_code/:supplier_code", (req, res) => {
  Hotel.findAll({
    where: {
      hotelSupplierCode: req.params.supplier_code,
      hotelPlaceCountryCode: req.params.country_code
    }
  })
    .then(hotels => {
      res.json(hotels);
    })
    .catch(err => console.log(err));
});

router.get("/getDestination/:country_code/:supplier_code", (req, res) => {
  Hotel.findAll({
    attributes: [
      [
        sequelize.fn("DISTINCT", sequelize.col("hotel_place_destination_code")),
        "hotelPlaceDestinationCode"
      ]
    ],
    where: {
      hotelPlaceCountryCode: req.params.country_code,
      hotelSupplierCode: req.params.supplier_code
    }
  }).then(hotels => {
    res.json(hotels);
  });
});

router.get(
  "/getDestinationCode/:destination_code/:supplier_code/:country_code",
  (req, res) => {
    Hotel.findAll({
      where: {
        hotelPlaceCountryCode: req.params.country_code,
        hotelSupplierCode: req.params.supplier_code,
        hotelPlaceDestinationCode: req.params.destination_code
      },
      // limit: 20,
      attributes: ["hotel_name", "hotel_code", "bind_id"]
    })
      .then(hotels => {
        res.json(hotels);
      })
      .catch(err => console.log(err));
  }
);

router.get("/getMatchHotel/:bind_id", (req, res) => {
  Hotel.findAll({
    where: {
      bindId: req.params.bind_id
    },
    attributes: ["hotel_name", "hotel_code", "bind_id", "hotel_supplier_code"]
  })
    .then(hotels => {
      res.json(hotels);
    })
    .catch(err => console.log(err));
});

module.exports = router;
